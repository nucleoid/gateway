const nucleoid = require("./nucleoid");
const token = require("./token");
const macro = require("./macro");
const STRING = macro.STRING;
const AuthorizationError = require("./error").AuthorizationError;
const uuid = require("uuid").v4;

module.exports.register = function(app) {
  app.get("/users/", (req, res, next) => {
    token
      .verify(req.get("authorization"), "ADMIN")
      .then(payload => {
        let n = `Users.filter(u => u.organization == ${payload.org})`;

        nucleoid.run("account", n).then(response => {
          res.status(response.status).send(response.data);
        });
      })
      .catch(e => {
        next(e);
      });
  });

  app.get("/users/:user", (req, res, next) => {
    token
      .verify(req.get("authorization"))
      .then(payload => {
        let user = req.params.user;

        if (payload.usr !== user) {
          throw new AuthorizationError();
        }

        let n = `${user}`;

        nucleoid.run("account", n).then(response => {
          res.status(response.status).send(response.data);
        });
      })
      .catch(e => {
        next(e);
      });
  });

  app.post("/users/:user", (req, res, next) => {
    token
      .verify(req.get("authorization"))
      .then(payload => {
        let data = req.body;
        let user = req.params.user;

        if (payload.usr !== user) {
          throw new AuthorizationError();
        }

        let n = `
        let firstName = ${STRING(data.first_name)};
        let lastName = ${STRING(data.last_name)};

        if(!firstName || !lastName) {
          throw "INVALID_USER"
        }

        ${user}.firstName = firstName;
        ${user}.lastName = lastName;`;

        nucleoid.run("account", n).then(response => {
          res.status(response.status).send(response.data);
        });
      })
      .catch(e => {
        next(e);
      });
  });

  app.post("/users/:user/groups/", (req, res, next) => {
    token
      .verify(req.get("authorization"), "ADMIN")
      .then(payload => {
        let data = req.body;
        let user = req.params.user;

        let n = `
        let groups = ${JSON.stringify(data)};

        if(!Array.isArray(groups)) {
          throw "INVALID_GROUP"
        }

    ${user}.groups = groups;`;

        nucleoid.run("account", n).then(response => {
          res.status(response.status).send(response.data);
        });
      })
      .catch(e => {
        next(e);
      });
  });

  app.post("/users/:user/password", (req, res, next) => {
    token
      .verify(req.get("authorization"))
      .then(payload => {
        let data = req.body;
        let user = req.params.user;

        if (payload.usr !== user) {
          throw new AuthorizationError();
        }

        let n = `
        let password = ${user}.password;
        let oldPassword = hash(${STRING(data.old_password)});
        let newPassword = hash(${STRING(data.new_password)});

        if(!oldPassword || !newPassword) {
          throw "INVALID_PASSWORD"
        }

        checkPassword(oldPassword, password);
        ${user}.password = newPassword;`;

        nucleoid.run("account", n).then(response => {
          res.status(response.status).send(response.data);
        });
      })
      .catch(e => {
        next(e);
      });
  });

  app.post("/users/", (req, res) => {
    let data = req.body;
    let n = `
    let email = ${STRING(data.email)}.toLowerCase();
    let password = hash(${STRING(data.password)});
    let firstName = ${STRING(data.first_name)};
    let lastName = ${STRING(data.last_name)};

    if(!email || !firstName || !lastName) {
      throw "INVALID_USER"
    }

    let organization = new Organization(${STRING(data.organization)});
    let user = new User (organization, email, password);
    user.firstName = firstName;
    user.lastName = lastName;
    user.groups = ["ADMIN", "USER"];`;

    nucleoid.run("account", n).then(response => {
      res.status(response.status).send(response.data);
    });
  });

  app.get("/codes/:code", (req, res) => {
    let n = `
    let curr = Date.now();
    let code = Codes.find(c => c.code == ${STRING(
      req.params.code
    )} && curr < c.expire && !c.disabled);

    if(!code) {
      throw "INVALID_CODE"
    }

    code;`;

    nucleoid.run("account", n).then(response => {
      res.status(response.status).send(response.data);
    });
  });

  app.post("/users/:email/codes", (req, res) => {
    let n = `
    let curr = Date.now() + 3600000;
    let email = ${STRING(req.params.email)}.toLowerCase();

    if(!email) {
      throw "INVALID_USER"
    }

    let user = Users.find(u => u.email == email);

    if(!user) {
      throw "INVALID_USER"
    }

    let code = new Code(${STRING(uuid())}, user);
    code.expire = curr;

    event("USER_CODE_GENERATED", code);`;

    nucleoid.run("account", n).then(response => {
      res.status(response.status).send(response.data);
    });
  });

  app.post("/codes/:code/users/", (req, res) => {
    let data = req.body;

    let n = `
    let curr = Date.now();
    let code = Codes.find(c => c.code == ${STRING(
      req.params.code
    )} && curr < c.expire && !c.disabled);

    if(!code) {
      throw "INVALID_CODE"
    }

    code.disabled = true;

    let firstName = ${STRING(data.first_name)};
    let lastName = ${STRING(data.last_name)};

    if(!firstName || !lastName) {
      throw "INVALID_USER"
    }

    let user = code.user;
    user.firstName = firstName;
    user.lastName = lastName;
    user.password = hash(${STRING(data.password)});`;

    nucleoid.run("account", n).then(response => {
      res.status(response.status).send(response.data);
    });
  });

  app.post("/organizations/:organization/users", (req, res, next) => {
    token
      .verify(req.get("authorization"), "ADMIN")
      .then(payload => {
        let organization = req.params.organization;

        if (payload.org !== organization) {
          throw new AuthorizationError();
        }

        let data = req.body;

        let n = `
        let email = ${STRING(data.email)}.toLowerCase();

        if(!email) {
          throw "INVALID_USER"
        }

        let user = new User (${organization}, email);
        user.groups = ["USER"];`;

        nucleoid.run("account", n).then(response => {
          res.status(response.status).send(response.data);
        });
      })
      .catch(e => {
        next(e);
      });
  });

  app.post("/oauth/", (req, res) => {
    let data = req.body;

    let n = `
    let email = ${STRING(data.email)}.toLowerCase();
    let password = hash(${STRING(data.password)});

    if(!email) {
      throw "INVALID_USER"
    }

    Users.find(u=>u.email == email && u.password == password)`;

    nucleoid.run("account", n).then(response => {
      let user = response.data;

      if (response.status == 400 || !user) {
        res.status(400).send({ type: "INVALID_USER" });
        return;
      }

      let t = token.sign(user.organization.id, user.id, user.groups);
      res.send({ token: t });
    });
  });

  app.post("/tokens", (req, res, next) => {
    token
      .verify(req.get("authorization"), "ADMIN")
      .then(payload => {
        let data = req.body;

        let n = `
        new Token(${STRING(data.name)}, ${payload.org}, ${STRING(uuid())});`;

        nucleoid.run("account", n).then(response => {
          res.status(response.status).send(response.data);
        });
      })
      .catch(e => {
        next(e);
      });
  });

  app.all("*", function(req, res) {
    res.status(404).end();
  });

  app.use((err, req, res, next) => {
    if (err instanceof AuthorizationError) {
      res.status(401).end();
      return;
    }

    res.type("txt");
    res.status(500).send(err.stack);
  });
};

module.exports.nucleoid = nucleoid;
module.exports.token = token;
module.exports.macro = macro;

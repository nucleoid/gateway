const http = require("network").http;

module.exports.run = function(process, statement) {
  return http.post("http://localhost:8080", statement, { process });
};

function STRING(string) {
  if (string === undefined) {
    return undefined;
  }

  if (string === null) {
    return null;
  }

  return `"${string}"`;
}

module.exports.STRING = STRING;

const jwt = require("jsonwebtoken");
const JsonWebTokenError = require("jsonwebtoken").JsonWebTokenError;
const AuthorizationError = require("./error").AuthorizationError;
const nucleoid = require("./nucleoid");

const privateKey = "PRIVATE_KEY";

function sign(org, usr, grp) {
  return jwt.sign(
    {
      org,
      exp: Math.floor(Date.now() / 1000) + 60 * 60,
      iat: Math.floor(Date.now() / 1000),
      usr,
      grp
    },
    privateKey
  );
}

async function verify(header, group) {
  try {
    if (!header) {
      throw new AuthorizationError();
    }

    let parts = header.split(" ");

    if (parts.length == 2) {
      if (parts[0].toUpperCase() != "BEARER") {
        throw new AuthorizationError();
      }

      let token = parts[1];

      if (token.split(".").length === 3) {
        let payload = jwt.verify(token, privateKey);

        if (payload.grp && Array.isArray(payload.grp)) {
          if (group) {
            if (payload.grp.includes(group)) {
              return payload;
            }
          } else {
            return payload;
          }
        }
      } else {
        let n = `
        Tokens.find(t => t.token == "${token}" && t.status == "ACTIVE")`;

        let response = await nucleoid.run("account", n);

        if (response.data && response.status == 200) {
          let org = response.data.organization.id;
          let usr = response.data.id;
          let grp = response.data.groups;

          if (grp && Array.isArray(grp)) {
            if (group) {
              if (grp.includes(group)) {
                return { org, usr, grp };
              }
            } else {
              return { org, usr, grp };
            }
          }
        }
      }
    }

    throw new AuthorizationError();
  } catch (error) {
    if (error instanceof JsonWebTokenError) {
      throw new AuthorizationError();
    } else {
      throw error;
    }
  }
}

module.exports.sign = sign;
module.exports.verify = verify;
